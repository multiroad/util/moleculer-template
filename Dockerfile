FROM node:10.15.1-alpine

LABEL maintainer="Miroslav Kubal <mirik.kubal@gmail.com>"

RUN apk --no-cache add git

COPY . /var/www
WORKDIR /var/www

RUN yarn
