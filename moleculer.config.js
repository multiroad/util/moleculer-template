import molConfigMaker from 'multiroad-utils/moleculer-config';
import config from 'multiroad-utils/multiroad-config';

export default molConfigMaker({
  nodeID: config.MOLECULER_NODE_ID || `calculator:${String(+new Date()).slice(-5)}`,
  namespace: config.MOLECULER_NAMESPACE,
  transporter: {
    options: {
      url: config.NATS_URL,
      user: config.NATS_USERNAME,
      pass: config.NATS_PASSWORD,
    },
  },
  cacher: config.CACHE_ENGINE === 'Redis' ? {
    type: 'Redis',
    options: {
      redis: config.LOCAL_REDIS ? {
        host: config.CACHE_REDIS_HOST,
        port: config.CACHE_REDIS_PORT,
      } : {
        sentinels: [
          {
            host: config.CACHE_REDIS_HOST,
            port: config.CACHE_REDIS_PORT,
          },
        ],
        name: 'mymaster',
      },
    },
  } : 'Memory',
});
