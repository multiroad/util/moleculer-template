export default {
  /* REQUIRED */
  MOLECULER_NODE_ID: null,
  MOLECULER_NAMESPACE: 'development',
  //
  MOLECULER_LOG_FORMATTER: '',
  //
  NATS_URL: 'nats://0.0.0.0:4222',
  NATS_USERNAME: '',
  NATS_PASSWORD: '',
  /* END REQUIRED */

  // Флаг, указывающий, что нужно использовать редис не как HA
  LOCAL_REDIS: false,
  // Какой кеш использовать
  CACHE_ENGINE: 'Redis',
  // REDIS Cache
  CACHE_REDIS_HOST: 'localhost',
  CACHE_REDIS_PORT: 6379,
  // REDIS Queues
  QUEUE_REDIS_HOST: 'localhost',
  QUEUE_REDIS_PORT: 6379,
  // ARANGO Database
  DATABASE_PROTOCOL: 'http',
  DATABASE_HOST: 'localhost',
  DATABASE_PORT: 8529,
  DATABASE_NAME: '',
  DATABASE_USERNAME: '',
  DATABASE_PASSWORD: '',
  // POSTGRES Database
  PG_DATABASE_HOST: 'localhost',
  PG_DATABASE_PORT: 5432,
  PG_DATABASE_NAME: '',
  PG_DATABASE_USERNAME: '',
  PG_DATABASE_PASSWORD: '',
  // ELASTICSEARCH
  ELASTICSEARCH_HOST: 'localhost',
  ELASTICSEARCH_PORT: '9200',
  ELASTICSEARCH_USERNAME: '',
  ELASTICSEARCH_PASSWORD: '',
};
