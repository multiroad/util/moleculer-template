module.exports = {
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.json'],
    alias: {
      '~': __dirname,
    },
  },
};
